/**
 * 
 */
package coen283.p3;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author kaushik
 *
 */
public class ConnectionManager {

	private JedisPool pool;
	private static ConnectionManager instance;

	/**
	 * 
	 */
	private ConnectionManager() {
		 pool = new JedisPool(new JedisPoolConfig(),"localhost");
	}
	
	public static ConnectionManager getInstance() {
		if(instance==null) {
			instance= new ConnectionManager();
		}
		return instance;
	}
	
	public synchronized Jedis getConnection() {
		return new Jedis("localhost");
	}
	
	public synchronized Jedis getConnectionFromPool() {
		return pool.getResource();
	}
	
	/**
	 * Has to be called only during program termination
	 */
	public void  destroyPool() {
		pool.destroy();
	}

	public void releaseConnection() {
		destroyPool();
	}
}
