package coen283.p3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Logger;

import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.FlockCommand;
import net.fusejna.FuseException;
import net.fusejna.StructFlock.FlockWrapper;
import net.fusejna.StructFuseFileInfo.FileInfoWrapper;
import net.fusejna.StructStat.StatWrapper;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.fusejna.types.TypeMode.ModeWrapper;
import net.fusejna.types.TypeMode.NodeType;
import net.fusejna.util.FuseFilesystemAdapterFull;

public class MetadataFileSystem extends FuseFilesystemAdapterFull {
	static File f_mountPoint = new File("./target/mirrorFS"); 
	// set mount point (path to a blank folder)
	private final String mirroredFolder = "./target/mirrorFolder";
	private MetadataManager meta; 
	// set the path of a folder which you want to be mirrored.
	private final static String mountPoint = f_mountPoint.getAbsolutePath();
	
	

	public MetadataFileSystem() {
		super();
		meta = MetadataManager.getInstance();
		for(Handler h: Logger.getGlobal().getHandlers()) {
			Logger.getGlobal().removeHandler(h);
		}
	}

	public static void main(String args[]) throws FuseException {
		new MetadataFileSystem().log(false).mount(mountPoint);
	}

	@Override
	public int getattr(final String path, final StatWrapper stat) {
		File f = new File(mirroredFolder + path);

		// if current path is of file
		if (f.isFile()) {
			stat.setMode(NodeType.FILE, true, true, true, true, true, true,
					true, true, true);
			stat.size(f.length());
			stat.atime(f.lastModified() / 1000L);
			stat.mtime(0);
			stat.nlink(1);
			stat.uid(0);
			stat.gid(0);
			stat.blocks((int) ((f.length() + 511L) / 512L));
			return 0;
		}

		// if current file is of Directory
		else if (f.isDirectory()) {
			stat.setMode(NodeType.DIRECTORY);
			return 0;
		}

		return -ErrorCodes.ENOENT();
	}

	@Override
	public int read(final String path, final ByteBuffer buffer,
			final long size, final long offset, final FileInfoWrapper info) {

		Path p = Paths.get(mirroredFolder + path);
		try {
			byte[] data = Files.readAllBytes(p);
			if ((offset + size) > data.length) {
				buffer.put(data, (int) offset, data.length - (int) offset);
				return (int) size;
			} else {
				buffer.put(data, (int) offset, (int) size);
				return (int) size;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public int write(final String path, final ByteBuffer buf,
			final long bufSize, final long writeOffset,
			final FileInfoWrapper wrapper) {
		byte[] b = new byte[(int) bufSize];
		buf.get(b);

		try {
			FileOutputStream output = new FileOutputStream(mirroredFolder
					+ path, true);
			output.write(b);
			output.close();
			return (int) bufSize;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;
	}

	@Override
	public int opendir(final String path, final FileInfoWrapper info) {
		return 0;
	}

	@Override
	public int readdir(final String path, final DirectoryFiller filler) {
		// adding fillers to directory
		File f = new File(mirroredFolder + path);

		if (f.isDirectory()) {
			File[] fList = f.listFiles();
			for (File file : fList) {
				filler.add(file.toString());
			}

		}

		return 0;
	}

	@Override
	public int create(final String path, final ModeWrapper mode,
			final FileInfoWrapper info) {
		File f = new File(mirroredFolder + path);
		try {
			f.createNewFile();
			mode.setMode(NodeType.FILE, true, true, true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public int mkdir(final String path, final ModeWrapper mode) {
		File f = new File(mirroredFolder + path);
		if (f.exists()) {
			f = null;
			return -ErrorCodes.EEXIST();
		}

		else {
			f.mkdir();
		}

		return 0;
	}

	@Override
	public int open(final String path, final FileInfoWrapper info) {
		return 0;
	}

	@Override
	public int rename(final String path, final String newName) {
		File f = new File(mirroredFolder + path);
		File f1 = new File(mirroredFolder + newName);
		boolean bool = false;

		try {

			// renaming file or folder
			bool = f.renameTo(f1);
			System.out.print("File/folder renamed? " + bool + " Rename from "
					+ path + " To " + newName);

		} catch (Exception e) {
			// if any error occurs
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public int readlink(final String path, final ByteBuffer buffer,
			final long size) {
		/*
		 * Path link = Paths.get(mountPoint+path);
		 * 
		 * if (Files.isSymbolicLink(link)) {
		 * System.out.println("map get. symlink path= "
		 * +path+" Target file Path=" +mapSymLink.get(path)); byte[] b =
		 * mapSymLink.get(path).getBytes(Charset.forName("UTF-8"));
		 * buffer.put(b); }
		 */
		return 0;
	}

	@Override
	public int lock(final String path, final FileInfoWrapper info,
			final FlockCommand command, final FlockWrapper flock) {
		return 0;
		// return -ErrorCodes.ENOSYS();
	}

	@Override
	public int setxattr(String path, String xattr, ByteBuffer buf, long size,
			int flags, int position) {
		// TODO Auto-generated method stub
		byte[] attr = new byte[(int) (size + position)];
		buf.get(attr,position,(int)size);
		String value = new String(attr);
		System.out.println("Adding attribute..."+value);
		Path p = Paths.get(mirroredFolder + path);
		
		meta.insert(p.toString(), xattr, value);
		return 0;
//		return super.setxattr(path, xattr, buf, size, flags, position);
	}

	/* (non-Javadoc)
	 * @see net.fusejna.util.FuseFilesystemAdapterFull#beforeMount(java.io.File)
	 */
	@Override
	public void beforeMount(File mountPoint) {
		// close the DB connections
		ConnectionManager.getInstance().releaseConnection();
		super.beforeMount(mountPoint);
	}

	/* (non-Javadoc)
	 * @see net.fusejna.util.FuseFilesystemAdapterFull#getxattr(java.lang.String, java.lang.String, net.fusejna.XattrFiller, long, long)
	 */
	@Override
	public int getxattr(String path, String xattr, XattrFiller filler,
			long size, long position) {
		// TODO Auto-generated method stub
		Path p = Paths.get(mirroredFolder + path);
		String value = meta.getValue(p.toString(),xattr);
		if(value!=null){
			filler.set(value.getBytes());
		}else {
			//FIXME - Not returning proper error code in  MAC. TEST ON LINUX
//			return -ErrorCodes.firstNonNull(ErrorCodes.ENOATTR(), ErrorCodes.ENOATTR(), ErrorCodes.ENODATA());
//			return super.getxattr(path, xattr, filler, size, position);
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see net.fusejna.util.FuseFilesystemAdapterFull#removexattr(java.lang.String, java.lang.String)
	 */
	@Override
	public int removexattr(String path, String xattr) {
		return super.removexattr(path, xattr);
	}

	/* (non-Javadoc)
	 * @see net.fusejna.util.FuseFilesystemAdapterFull#listxattr(java.lang.String, net.fusejna.XattrListFiller)
	 */
	@Override
	public int listxattr(String path, XattrListFiller filler) {
//		System.out.println("list attr called");
		Path p = Paths.get(mirroredFolder + path);
		Map<String,String> map = meta.getMapValues(p.toString());
		filler.add(map.keySet());
		return 0;
	}
	
	
	
}
