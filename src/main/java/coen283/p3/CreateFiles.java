/**
 * 
 */
package coen283.p3;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author kaushik
 *
 */
public class CreateFiles {
	final File f_mountPoint = new File("./target/mirrorFS"); 
	/**
	 * 
	 */
	public CreateFiles() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		args=new String[] {"mf" ,"target/mirrorFS", "10", "1"};
		args=new String[] {"mf" ,"target/mirrorFS", "100000", "1"};
		if(args.length!=4) {
			System.out.println("CreateFiles <file-prefix> <target-dir> <total-files> <threads>");
			System.exit(-1);
		}
		String prefix=args[0];
		String targetDir=args[1];
		int totalFiles = Integer.parseInt(args[2]);
		int threads=Integer.parseInt(args[3]);

		final int partSize = totalFiles/threads;
		ExecutorService executor = Executors.newFixedThreadPool(threads);
		long startTime=System.currentTimeMillis();
		for(int i =1;i<=threads;i++) {
			System.out.println("initializing thread..."+i);
			executor.execute(new Worker(prefix,targetDir,partSize*i,partSize));
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		long runtime = System.currentTimeMillis()-startTime;
		System.out.println("Finished all threads: Time taken(s) - "+runtime/1000);
	}

}

class Worker implements Runnable{
	private int start,size;
	private String prefix;
	private String targetDir;

	public Worker(String prefix, String targetDir, int startNo, int partsize) {
		this.start=startNo;
		this.size=partsize;
		this.prefix = prefix;
		this.targetDir=targetDir;
		System.out.printf("Initializing...start=%s size=%s prefix=%s targetDir=%s\n",start,size,prefix,targetDir);
	}

	@Override
	public void run() {
		long threadId = Thread.currentThread().getId();
		System.out.println("Executing thread: "+threadId);
		for(int i = 1 ; i<=size;i++ ) {
			File f = new File(targetDir,prefix+(start+i)+".txt");
			try {
//				System.out.println("creating file: "+threadId+"-"+(start+i));
				f.createNewFile();
			} catch (IOException e) {
				System.err.println("Error creating file..."+targetDir+"/"+prefix+start+".txt");
			}
		}
		System.out.printf("Thread %d created %d files\n",threadId,size);
	}

}