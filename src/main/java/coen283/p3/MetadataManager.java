package coen283.p3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import redis.clients.jedis.Jedis;

public class MetadataManager {

	private static MetadataManager instance;
	private ConnectionManager connMgr;
	
	private MetadataManager() {
		connMgr = ConnectionManager.getInstance();
	}
	
	public static  MetadataManager getInstance() {
		if(instance==null) {
			instance = new MetadataManager();
		}
		return instance;
	}
	
	public boolean insert(String filename, String key, String value) {
		Jedis conn= connMgr.getConnection();
		Map<String, String> tagData = new HashMap<String,String>();
		tagData.put(key, value);
		conn.hmset(filename,tagData);
		// TODO - Add reverse mapping - LIST by name key::value
		conn.lpush(key+"::"+value, filename);
		return true;
	}
	
	public long delete(String filename, String key) {
		Jedis conn= connMgr.getConnection();
		String value = conn.hget(filename, key);
		long data = conn.hdel(filename,key);
		// TODO - Add reverse mapping - LIST by name key::value
		conn.lrem( key+"::"+value,1,filename);
		return data;
	}
	
	public boolean deleteAll(String filename) {
		Jedis conn= connMgr.getConnection();
		return true;
	}
	
	public boolean edit(String filename, String key, String value) {
		Jedis conn= connMgr.getConnection();
		conn.hset(filename,key,value);
		String oldValue = conn.hget(filename, key);
		//TODO - Add reverse mapping
		conn.lrem( key+"::"+oldValue,1,filename);
		//update new value
		conn.lpush(key+"::"+value, filename);
		
		return true;
	}
	
	public boolean addToList(String keyValueListName, String value) {
		Jedis conn= connMgr.getConnection();
		// Add to list
		return true;
	}
	
	public Map<String,String> getMapValues(String name) {
		Jedis conn = connMgr.getConnection();
		return conn.hgetAll(name);
	}
	
	public String getValue(String map, String field) {
		Jedis conn = connMgr.getConnection();
		List<String> list = conn.hmget(map, field);
		return (list!=null&& list.size()>0)?list.get(0):null;
	}
}
