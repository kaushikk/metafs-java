## METADATA FILE SYSTEM BASED ON JNA ##

### Quick Summary ###

** COMPILE**


```
#!shell

./gradle rtJar
```


** Create sample files** 

```
#!shell

 ./create_file.sh target/mirrorFS/ 1 999
```

This creates files named from file1.txt to file999.txt

**Create Mount Point**

```
#!shell

mkdir target/mirrorFS
```

**Start FS**


```
#!shell

./run.sh
```


### Summary of set up ###

To start file system 

1. create folder target/mirrorFS
2. ./gradlew rtJar

### How to run tests ###

1. Add an attribute

	
```
#!shell

xattr -w <attr_name> <attr_value> filename

	eg: xattr -w owner kaushik target/mirrorFS/file1.txt
```


2. List attribute names present 

	
```
#!shell

xattr -p owner target/mirrorFS/file1.txt
```


3. List All Attributes
	
```
#!shell

xattr -l owner target/mirrorFS/file1.txt
```


### Run the program using ###
	

```
#!shell

./run.sh
```

Attributes can be added using xattr command

### TODO ###

-> getattr - DONE

-> removeattr

-> editattr -> not yet tested

-> querying

-> documentation -> presentation -> readme

-> script

-> bench marking



-> removing a file from the system -> update the database accordingly